//
//  SidePanelController.m
//  Backspace Table
//
//  Created by Dave Heere on 25/04/14.
//  Copyright (c) 2014 Epicúreo. All rights reserved.
//

#import "SidePanelController.h"
#import "JASidePanelController.h"

@interface SidePanelController ()

@end

@implementation SidePanelController

-(void)awakeFromNib
{
    self.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"MapsPanel"];
    self.leftPanel =   [self.storyboard instantiateViewControllerWithIdentifier:@"MenuPanel"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = YES;
    JASidePanelController *sidepPanel = [[JASidePanelController alloc]init];
    sidepPanel.recognizesPanGesture = YES;
 

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
