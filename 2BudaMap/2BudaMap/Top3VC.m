//
//  Top3VC.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "Top3VC.h"

@interface Top3VC ()

@property (nonatomic) NSArray *opties2;


@end

@implementation Top3VC




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.headerTop3Image.text = self.selectedTitleOpties;
    
    self.opties2 = [[VenueStore sharedStore] giveTopVenuesForSubtype:self.selectedTitleOpties];


    //NSLog(@"self.selectedTitleOpties %@", self.selectedTitleOpties);

    //NSLog(@"self.opties2 %@", self.opties2);


    
    
    NSArray *alles = [[VenueStore sharedStore] venueList];
    [self addVenuesToMap:alles];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"number of rows: %d", [self.opties2 count]);
    return [self.opties2 count];
}


//setup cells in tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //setup cell
    static NSString *CellIdentifier = @"top3Celltje";
    Top3Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    VenueItem *item = [self.opties2 objectAtIndex:indexPath.row];
    
    cell.headerTop3Image.text = item.title;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationItem.title = self.selectedTitleOpties;
    
    _headerTop3Name.text = _selectedTitleOpties;

    cell.imageViewTop3.image = item.venuePhoto;

    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"cell tapped top 3");

    [self performSegueWithIdentifier:@"pushToInfo" sender:self];
    
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.top3View indexPathForSelectedRow];
    VenueItem *venue = [self.opties2 objectAtIndex:selectie.row];
    
    InfoKaartjeVC *segueDestination = segue.destinationViewController;
    segueDestination.myVenue = venue;
    
    
}




/*
#pragma mark - Navigation
 In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
