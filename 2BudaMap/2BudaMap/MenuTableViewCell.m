//
//  MenuTableViewCell.m
//  2BudaMap
//
//  Created by Dave Heere on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
