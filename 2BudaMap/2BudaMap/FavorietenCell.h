//
//  FavorietenCell.h
//  2BudaMap
//
//  Created by Dave Heere on 30/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavorietenCell.h"

@interface FavorietenCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *favorietenNaamCell;


@end
