//
//  Top3Cell.h
//  2BudaMap
//
//  Created by Dave Heere on 27/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Top3VC.h"


@interface Top3Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewTop3;
@property (weak, nonatomic) IBOutlet UILabel *headerTop3Image;


@end
