//
//  SidePanelController.h
//  Backspace Table
//
//  Created by Dave Heere on 25/04/14.
//  Copyright (c) 2014 Epicúreo. All rights reserved.
//

#import "JASidePanelController.h"
#import "MenuVC.h"
#import "MenuTableViewCell.h"

@interface SidePanelController : JASidePanelController

@end
