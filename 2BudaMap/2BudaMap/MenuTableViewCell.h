//
//  MenuTableViewCell.h
//  2BudaMap
//
//  Created by Dave Heere on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeCell.h"

@interface MenuTableViewCell : UITableViewCell
// menu cell show iets
@property (weak, nonatomic) IBOutlet UIImageView *menuIcon;
@property (weak, nonatomic) IBOutlet UILabel *menuTitle;



@end
