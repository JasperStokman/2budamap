//
//  VenueItem.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "VenueItem.h"

@implementation VenueItem

+ (instancetype) createNepVenue;
{
    VenueItem *newItem = [[VenueItem alloc] init];
    newItem.title = @"title";
    newItem.subType = @"bar";
    return newItem;
    
    
}

- (UIImage *)venuePhoto
{
    NSString *titleName = [self.title.lowercaseString stringByAppendingPathExtension:@"jpg"];
    UIImage *photoName = [UIImage imageNamed:titleName];
    
    return  photoName;
    
}

- (NSString *)pincolor{
    return _pinType;
}

- (void) setpincolor:(NSString*) String1{
    _pinType = String1;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
         self.omschrijving = [aDecoder decodeObjectForKey:@"omschrijving"];
        self.telefoon = [aDecoder decodeObjectForKey:@"telefoon"];
        self.website = [aDecoder decodeObjectForKey:@"website"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        
        
//        self.teamID = [aDecoder decodeObjectForKey:@"teamID"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.omschrijving forKey:@"omschrijving"];
        [aCoder encodeObject:self.telefoon forKey:@"telefoon"];
    [aCoder encodeObject:self.website forKey:@"website"];
    [aCoder encodeObject:self.email forKey:@"email"];
 

//    [aCoder encodeObject:self.teamNaam forKey:@"teamNaam"];
    
}



@end
