//
//  MenuDetailVC.h
//  2BudaMap
//
//  Created by Dave Heere on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuDetailVC : UIViewController

@property (nonatomic, strong) NSString *selectedTitle;


@end
