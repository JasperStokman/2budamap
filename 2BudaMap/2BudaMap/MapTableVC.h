//
//  MapTableVC.h
//  
//
//  Created by Dave Heere on 26/05/14.
//
//
#import <MapKit/MapKit.h>
#import "VenueItem.h"
@interface MapTableVC : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate>

@property (nonatomic, strong) VenueItem *presentVenueItem;
@property (strong, nonatomic) IBOutlet UITableView *homeTableView;


@property (weak, nonatomic) IBOutlet UIButton *mapUpBTN;
@property (weak, nonatomic) IBOutlet UIButton *mapDownBTN;


- (IBAction)mapUp:(id)sender;
- (IBAction)mapDown:(id)sender;



@property (weak, nonatomic) IBOutlet UIView *listViewOverlay;




@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *testLabelOptie;


@property (weak, nonatomic) IBOutlet UIImageView *imageViewTop3;
@property (weak, nonatomic) IBOutlet UILabel *headerTop3Image;

- (IBAction)currentLocationBTN:(id)sender;



@property (weak, nonatomic) IBOutlet UIButton *currentLocation;



- (void) addVenuesToMap:(NSArray *)venues;

@property (strong, nonatomic) MKMapView *deMapView;

@property (weak, nonatomic) IBOutlet UIImageView *menubalkOnder;


@property (weak, nonatomic) IBOutlet UIImageView *top3Photo;








@end
