//
//  OptieVC.m
//  2BudaMap
//
//  Created by Dave Heere on 26/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "OptieVC.h"
#import "MapTableVC.h"
#import "Top3VC.h"



@interface OptieVC ()
@property (nonatomic) NSArray *opties;
@end

@implementation OptieVC

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sectionss.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.opties count];
}


//setup cells in tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //setup cell
    static NSString *CellIdentifier = @"OptieCelltje";
    OptieCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.optieLabel.text = [self.opties objectAtIndex:indexPath.row];
    
    // cell.homeCellLabel.text = [TESTDAVEDATE objectAtIndex:indexPath.row];
    //cell.iconShow.image = [ImageStore categoryImageAtIndex:indexPath.row];
    
    //    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"cell tapped");
    
   [self performSegueWithIdentifier:@"pushToTop3" sender:self];

    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.homeTableView indexPathForSelectedRow];
    NSString *title = [self.opties objectAtIndex:selectie.row];

    if ([segue.identifier isEqualToString:@"pushToInfo"]) {
        InfoKaartjeVC *segueDestination = segue.destinationViewController;        segueDestination.myVenue = self.presentVenueItem;

    } else {
        Top3VC *segueDestination = segue.destinationViewController;
 
        segueDestination.selectedTitleOpties = title;
   }
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.testLabelOptie.text = self.selectedTitle;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationItem.title = self.selectedTitle;

    


    self.opties = [[VenueStore sharedStore] giveSubtypesForCategory:self.selectedTitle];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    
    NSArray *alles = [[VenueStore sharedStore] venueList];
    [self addVenuesToMap:alles];


    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
