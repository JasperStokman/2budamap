//
//  Top3VC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapTableVC.h"
#import "HomeListTableVC.h"
#import "OptieVC.h"
#import "VenueItem.h"
#import "VenueStore.h"
#import "Top3Cell.h"
#import "InfoKaartjeVC.h"

@interface Top3VC :  MapTableVC

@property (strong, nonatomic) IBOutlet UITableView *top3View;


@property (nonatomic) NSString *selectedTitleOpties;
@property (nonatomic) NSString *top3Title;



@property (weak, nonatomic) IBOutlet UILabel *headerTop3Name;



@end
