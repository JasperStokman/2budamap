//
//  FavorietenTVC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenueItem.h"
#import "VenueStore.h"
#import "FavorietenCell.h"
#import "InfoKaartjeVC.h"
#import "MapTableVC.h"
#import "Top3VC.h"
@interface FavorietenTVC : UITableViewController <UITableViewDelegate, UITableViewDataSource>



@property (strong, nonatomic) IBOutlet UITableView *favorietView;
@property (nonatomic, strong) VenueItem *presentVenueItem;

@property (nonatomic) NSString *selectedTitleFavo;
@property (strong, nonatomic) IBOutlet UITableView *tableView;





@end
