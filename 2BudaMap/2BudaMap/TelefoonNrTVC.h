//
//  TelefoonNrTVC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "telefoonCell.h"
#import "VenueStore.h"


@interface TelefoonNrTVC : UITableViewController <UITableViewDelegate, UITableViewDataSource> 

@property (strong, nonatomic) IBOutlet UITableView *telephoneTableView;



@end
