//
//  InstellingenVC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstellingenVC : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
