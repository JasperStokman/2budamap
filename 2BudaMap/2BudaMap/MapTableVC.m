//
//  MapTableVC.m
//  
//
//  Created by Camille Bruma on 26/05/14.
//
//
#import "VenueItem.h"
#import "MapTableVC.h"
#import <MapKit/MapKit.h>
#import "InfoKaartjeVC.h"
#import "VenueStore.h"
#define CATEGORIES [[VenueStore sharedStore] categories]
#define HOMEICONS [ImageStore categoriesIcons]
#define METERS_PER_MILE 1609.344

@implementation MapTableVC



@synthesize mapView = _mapView;



    



- (void) viewDidLoad
{
    [super viewDidLoad];
     
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    [self.homeTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
   
    _mapView.showsUserLocation=TRUE;
    
    // zoom to  a specific area
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 47,0000;
    zoomLocation.longitude = 19,0000;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 25*METERS_PER_MILE, 25*METERS_PER_MILE);
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
    
    // make sure the Google water mark is always visible
    _mapView.autoresizingMask =
    (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    [_mapView setRegion:adjustedRegion animated:YES];
    
    _mapView.delegate=self;
    
    
    self.mapView.delegate=self;
    [self.mapView setShowsUserLocation:YES];
    
    [UIView animateWithDuration:0.1 animations:^{
        _currentLocation.alpha = 0.0;
    }];
    
 
    self.deMapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
   
    
    

}



//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    CLLocationCoordinate2D loc = [userLocation coordinate];
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 10000, 10000);
//    [self.mapView setRegion:region animated:YES];
//}

#warning De map opent nu op Userlocation. Willen we dat veranderen naar Budapest, moeten we onderstaande code toevoegen!
    
//    CLLocationCoordinate2D coord = {.latitude =  47,614292, .longitude =  19,0524, };
//    MKCoordinateSpan span = {.latitudeDelta =  0.2, .longitudeDelta =  0.2};
//    MKCoordinateRegion region = {coord, span};
//    
//    [self.deMapView setRegion:region];
//    [self.view addSubview:self.deMapView];
    




- (IBAction)currentLocationBTN:(id)sender {
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    
    
    
}


// Maplist omhoog
- (IBAction)mapUp:(id)sender {
    
    
    UILabel *label = [[UILabel alloc]init];
    label.userInteractionEnabled = YES;
    
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(labelDragged:)];
    [label addGestureRecognizer:gesture];
    
    [self.view addSubview:label];
    
    
    [UIView animateWithDuration:1.6 animations:^{
        _menubalkOnder.frame = CGRectMake(0, 0, 320, 57);
        _menubalkOnder.alpha = 0.0;
        
    }];
    
    
    
    [UIView animateWithDuration:1.2 animations:^{
        
        //  _loginOverlayView.frame = self.view.frame;
        _listViewOverlay.frame = CGRectMake(0, 168, 320, 401);
        _listViewOverlay.layer.masksToBounds = YES;

        _listViewOverlay.layer.masksToBounds=YES;
        
    }];
    
    
   
    [UIView animateWithDuration:0.6 animations:^{
        _mapDownBTN.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:1.2 animations:^{
        _mapUpBTN.alpha = 0.0;

    }];
    
    // Maps kleiner maken
    [UIView animateWithDuration:1.2 animations:^{
        
        //  _loginOverlayView.frame = self.view.frame;
        _mapView.frame = CGRectMake(0, 0, 320, 200);
        _mapView.layer.masksToBounds = YES;
        //        _listViewOverlay.layer.borderColor=[[UIColor whiteColor]CGColor];
        //        _listViewOverlay.layer.borderWidth= 1.0f;
        //        _listViewOverlay.layer.cornerRadius=5.0f;
        _mapView.layer.masksToBounds=YES;
        
    }];
    
    [UIView animateWithDuration:1.6 animations:^{
        _currentLocation.frame = CGRectMake(7, 0, 45, 45);
        _currentLocation.alpha = 0.0;
    }];
    
}

- (void)labelDragged:(UIPanGestureRecognizer *)gesture
{
	UILabel *label = (UILabel *)gesture.view;
	CGPoint translation = [gesture translationInView:label];
    
	// move label
	label.center = CGPointMake(label.center.x + translation.x,
                               label.center.y + translation.y);
    
	// reset translation
	[gesture setTranslation:CGPointZero inView:label];
}


// Maplist naar beneden
- (IBAction)mapDown:(id)sender {
    
   
    
    [UIView animateWithDuration:1.3 animations:^{
            _menubalkOnder.frame = CGRectMake(0, 515, 320, 57);
            _menubalkOnder.alpha = 1.0;

    }];
    
    
    
    [UIView animateWithDuration:1.2 animations:^{
        
        _listViewOverlay.frame = CGRectMake(0, 513, 320, 401);
        _listViewOverlay.layer.masksToBounds = YES;
        _listViewOverlay.layer.masksToBounds=YES;
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        _mapDownBTN.alpha = 0.0;
    }];
    [UIView animateWithDuration:0.6 animations:^{
        _mapUpBTN.alpha = 1.0;
    }];
    
    // maakt de mapview weer groot (fullscreen)
    [UIView animateWithDuration:1.2 animations:^{
        
        //  _loginOverlayView.frame = self.view.frame;
        _mapView.frame = CGRectMake(0, 0, 320, 600);
        _mapView.layer.masksToBounds = YES;
        //        _listViewOverlay.layer.borderColor=[[UIColor whiteColor]CGColor];
        //        _listViewOverlay.layer.borderWidth= 1.0f;
        //        _listViewOverlay.layer.cornerRadius=5.0f;
        _mapView.layer.masksToBounds=YES;
        
    }];
    
    
// current location butten laten zien
    [UIView animateWithDuration:1.5 animations:^{
        _currentLocation.frame = CGRectMake(7, 517, 45, 45);
        _currentLocation.alpha = 1.0;
    }];

   
    
}



- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *dePin = @"dePin";
    
    MKPinAnnotationView *customPinView = [[MKPinAnnotationView alloc]
                                          initWithAnnotation:annotation reuseIdentifier:dePin];
    customPinView.pinColor = MKPinAnnotationColorRed;
    customPinView.animatesDrop = YES;
    customPinView.canShowCallout = YES;
    //customPinView.image = [UIImage imageNamed:@"pointer_drinkskopie_30.png"];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    customPinView.rightCalloutAccessoryView = rightButton;
    
    VenueItem *testVenue = annotation;
    //NSLog(@"location: %f %f",testVenue.coordinate.longitude,testVenue.coordinate.latitude);
    
    if (annotation == _mapView.userLocation){
        //customPinView.image = [UIImage imageNamed:@"pointmap.png"];
        customPinView.image = [UIImage imageNamed:@"pointmap.png"];
        customPinView.enabled = NO;
    }
    else if ([testVenue.venueType isEqualToString:@"DRINK"]) {
        customPinView.image = [UIImage imageNamed:@"mappointer_drinks1.png"];
        customPinView.enabled = YES;
    }
    else if([testVenue.venueType isEqualToString:@"EAT"]) {
        customPinView.image = [UIImage imageNamed:@"mappointer_food1.png"];
        
    }
    else if ([testVenue.venueType isEqualToString:@"PARTY "]) {
        customPinView.image = [UIImage imageNamed:@"mappointer_party1.png"];
    }
    else if ([testVenue.venueType isEqualToString:@"LIVE MUSIC"]){
        customPinView.image = [UIImage imageNamed:@"mappointer_livemusic1.png"];
    }
    else if ([testVenue.venueType isEqualToString:@"SHOPS"]) {
        customPinView.image = [UIImage imageNamed:@"mappointer_shopping1.png"];
    }
    
    else if ([testVenue.venueType isEqualToString:@"POOLS & SPAS"]){
        customPinView.image = [UIImage imageNamed:@"mappointer_spa.png1"];
    }

    
    return customPinView;
    
}

    
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
   
    
    // here we illustrate how to detect which annotation type was clicked on for its callout
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[VenueItem class]])
    {
        self.presentVenueItem = annotation;
       // NSLog(@"clicked infoknopje");
    }
    
    [self performSegueWithIdentifier:@"pushToInfo" sender:self];

//
//    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

// deze wordt nooit aangeroepen?
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.homeTableView indexPathForSelectedRow];
    NSString *title = [CATEGORIES objectAtIndex:selectie.row];
    //NSLog(@"%@", title);
    
    if ([segue.identifier isEqualToString:@"pushToInfo"]) {
        InfoKaartjeVC *segueDestination = segue.destinationViewController;
        segueDestination.myVenue = self.presentVenueItem;
        
    } else {
        OptieVC *segueDestination = segue.destinationViewController;
        segueDestination.selectedTitle = title;
    }
//    NSLog(@"%@", venue);
    
    
}
    
- (void)viewDidUnload
{

[self setMapView:nil];
    [super viewDidUnload];
    
   
    
}



- (void) addVenuesToMap:(NSArray *)venues
{
    for (VenueItem *venue in venues) {
        [self.mapView addAnnotation:venue];
    }
    
}








@end
