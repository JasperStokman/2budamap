//
//  homeListTableVC.m
//  2BudaMap
//
//  Created by Dave Heere on 23/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "homeListTableVC.h"


#define CATEGORIES [[VenueStore sharedStore] categories]
#define HOMEICONS [ImageStore categoriesIcons]

@interface HomeListTableVC ()



@end

@implementation HomeListTableVC



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[VenueStore sharedStore] categories] count];;
}


//setup cells in tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //setup cell
    static NSString *CellIdentifier = @"homeCelltje";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSArray *test = [[VenueStore sharedStore]categories];
    
    cell.homeCellLabel.text = [test objectAtIndex:indexPath.row];
    cell.iconShow.image = [ImageStore categoryImageAtIndex:indexPath.row];

//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
 
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"cell tapped");
    
    [self performSegueWithIdentifier:@"categoryToOptie" sender:self];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *alles = [[VenueStore sharedStore] venueList];
   
    [self addVenuesToMap:alles];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationItem.title = @"CATEGORY";
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.homeTableView indexPathForSelectedRow];
    NSString *title = [CATEGORIES objectAtIndex:selectie.row];
    
   
    
    if ([segue.identifier isEqualToString:@"pushToInfo"]) {
        InfoKaartjeVC *segueDestination = segue.destinationViewController;
        segueDestination.myVenue = self.presentVenueItem;
        

    } else {
        OptieVC *segueDestination = segue.destinationViewController;
        segueDestination.selectedTitle = title;
        
     
    }
}




@end
