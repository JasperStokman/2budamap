//
//  optieCell.h
//  2BudaMap
//
//  Created by Dave Heere on 26/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptieVC.h"

@interface OptieCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *optieLabel;

@end
