//
//  OptieVC.h
//  2BudaMap
//
//  Created by Dave Heere on 26/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptieCell.h"
#import "VenueStore.h"
#import "MapTableVC.h"
#import "Top3VC.h"


@interface OptieVC : MapTableVC

@property (nonatomic) NSString *selectedTitle;


@end
