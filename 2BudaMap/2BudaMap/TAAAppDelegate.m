//
//  TAAAppDelegate.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "TAAAppDelegate.h"
#import "JASidePanelController.h"
#import "SidePanelController.h"
#import "MenuDetailVC.h"
#import "homeListTableVC.h"
#import "MapVC.h"
#import "InfoKaartjeVC.h"
#import "VenueStore.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:0.2]

@implementation TAAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor blackColor];
    
    // kleur van de navigatie bar veranderen.....
   // [[UINavigationBar appearance] setBarTintColor:[UIColor redColor]];

   // [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xC3000B)];
    // RGB KLEUREN :::: https://kuler.adobe.com/ //
    
    UIImage *navBackgroundImage = [UIImage imageNamed:@"infoKaartBalk-1_C3000B.png"];
    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];

    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



//- (void)applicationDidEnterBackground:(UIApplication *)application
//{
//    BOOL success = [[VenueStore sharedStore] saveChanges];
//    if (success) {
//        NSLog(@"Saved all of the favorite items");
//    }
//    else {
//        NSLog(@"Could not save any of the favorite items");
//    }
//}









- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
