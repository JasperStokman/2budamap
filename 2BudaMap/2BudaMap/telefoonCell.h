//
//  telefoonCell.h
//  2BudaMap
//
//  Created by Dave Heere on 23/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TelefoonNrTVC.h"

@interface telefoonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *telephoneNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *telephoneNumberLabel;
- (IBAction)callThisNumber:(id)sender;

@end
