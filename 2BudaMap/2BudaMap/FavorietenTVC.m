//
//  FavorietenTVC.m
//  2BudaMap
//
//  Created by Jasper Stokman on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "FavorietenTVC.h"

@interface FavorietenTVC ()

@property (nonatomic) NSArray *favorietenArray;


@end

@implementation FavorietenTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDir stringByAppendingPathComponent:@"savedVenue.plist"];
    
    
    NSData *archiveData = [NSData dataWithContentsOfFile:fullPath];
    
    
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:archiveData];
    
    NSLog(@"dit is een array van favoTVC: %@", array);
    
    self.favorietenArray = [[NSMutableArray alloc]initWithArray:array];
    
    [self.tableView reloadData];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationItem.title = @"Favorites";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

 //   self.navigationItem.rightBarButtonItem = self.editButtonItem;

    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"aantal favorieten: %d", self.favorietenArray.count);
    return self.favorietenArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //setup cell
  NSString *CellIdentifier = @"favorietCelltje";
    
    
//    FavorietenCell *cell = [[FavorietenCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
   FavorietenCell *cell = (FavorietenCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if ((cell == nil)) {
//        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"favorietCelltje" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
    
        
    
    
//    VenueItem *favo = self.favoss[indexPath.row];
    VenueItem *favo = [self.favorietenArray objectAtIndex:indexPath.row];
    
    
    
    cell.favorietenNaamCell.text = favo.title;
    
   
    
        NSLog(@"favo en favo.title: %@ %@", favo, favo.title);
    
    NSLog(@"dit is Cell en cell.favorieten.text: %@, %@", cell, cell.textLabel.text);
    //VenueStore *favo = [FAVORIETEN objectAtIndex:indexPath.row];
    
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"cell tapped top 3");
    
    [self performSegueWithIdentifier:@"pushToInfo" sender:self];
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.favorietView indexPathForSelectedRow];
    VenueItem *venue = self.favorietenArray [selectie.row];
    
//    NSLog(@"%@", venue);
    
    InfoKaartjeVC *segueDestination = segue.destinationViewController;
    segueDestination.myVenue = venue;
}



// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
////        [FAVORIETEN removeObjectAtIndex:indexPath.row];
//        [self.favoss removeObjectAtIndex:indexPath.row];
//        
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}


@end
