//
//  VenueItem.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>




@interface VenueItem : NSObject <MKAnnotation>
@property (nonatomic, retain) NSString *pinType;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *venueType;
@property (nonatomic, copy) NSString *subType;
@property (nonatomic, copy) NSString *adres;
@property (nonatomic, copy) NSString *telefoon;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *omschrijving;
@property (nonatomic, copy) NSString *openingsTijden;
@property (nonatomic) CLLocation *locatie;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *website;

+(instancetype) createNepVenue;
- (UIImage *)venuePhoto;


@end
