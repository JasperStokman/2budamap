//
//  homeCell.h
//  2BudaMap
//
//  Created by Dave Heere on 23/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeListTableVC.h"

@interface HomeCell : UITableViewCell
// laat iets zien op table view label.. home/ maps...
@property (weak, nonatomic) IBOutlet UILabel *homeCellLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconShow;

@end
