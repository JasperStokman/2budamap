//
//  MenuVC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuTableViewCell.h"
#import "SidePanelController.h"
#import "MenuTableViewCell.h"
#import "JASidePanelController.h"

@interface MenuVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

// laat het menu zien in een table view
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (strong, nonatomic) IBOutlet UITextField *showDetail;


@end
