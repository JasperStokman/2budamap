//
//  InfoKaartjeVC.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenueStore.h"
#import "VenueItem.h"
#import "Top3Cell.h"
#import "Top3VC.h"

@interface InfoKaartjeVC : UIViewController <NSCoding>

@property (nonatomic) NSString *selectedTitleTop3;

@property (nonatomic) VenueItem *myVenue;
@property (nonatomic) VenueItem *item;
@property (nonatomic) NSData *archiveData;

@property (weak, nonatomic) IBOutlet UILabel *tekstAchtergrondjeTest;

@property (weak, nonatomic) IBOutlet UILabel *infoAdres;
@property (weak, nonatomic) IBOutlet UILabel *infoTelefoonNummer;
@property (weak, nonatomic) IBOutlet UILabel *infoEmail;
@property (weak, nonatomic) IBOutlet UILabel *infoWebsite;

@property (weak, nonatomic) IBOutlet UILabel *infoHeaderLabel;

@property (weak, nonatomic) IBOutlet UITextView *infoOmschrijving;
- (IBAction)callInfoCard:(id)sender;

- (IBAction)addToFavoInfoCard:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *hideFavoButton;

@property (weak, nonatomic) IBOutlet UIImageView *infoKaartjePhoto;



@end
