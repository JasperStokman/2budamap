//
//  ImageStore.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "ImageStore.h"

@implementation ImageStore





+ (NSArray *)categoriesIcons
{
    return [NSArray arrayWithObjects:@"icons_party.png", @"icons_livemusic.png",@"icons_drink.png",@"icons_spa.png",@"icons_shops.png",@"icons_eat.png",@"icons_spa.png",@"icons_transport.png",  nil];
}

+ (UIImage *) categoryImageAtIndex:(NSInteger) index
{
    return [UIImage imageNamed:[[self categoriesIcons] objectAtIndex:index]];
}

@end
