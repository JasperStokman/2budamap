//
//  InfoKaartjeVC.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "InfoKaartjeVC.h"


#define FAVOS [[VenueStore sharedStore] favorieten]


@interface InfoKaartjeVC ()

@property (nonatomic) NSMutableArray *favVenues;

@end

@implementation InfoKaartjeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;

}




- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.infoAdres.text = self.myVenue.adres;
    self.infoEmail.text = self.myVenue.email;
    self.infoOmschrijving.text = self.myVenue.omschrijving;
    self.infoTelefoonNummer.text = self.myVenue.telefoon;
    self.infoWebsite.text = self.myVenue.website;
    self.infoHeaderLabel.text = self.myVenue.title;
    
    self.infoKaartjePhoto.image = self.myVenue.venuePhoto;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [UIFont fontWithName:@"Novecentowide-Medium" size:21];
    self.navigationItem.title = self.myVenue.subType;

   
    // favo button aan uit ?
    
[self.hideFavoButton setBackgroundImage:[UIImage imageNamed:@"favorite_off_106x43_.png"] forState:UIControlStateNormal];
   [self.hideFavoButton setBackgroundImage:[UIImage imageNamed:@"favorite_on_106x43_.png"] forState:UIControlStateSelected];
    
// #warning Dit erbij gevoegd dus
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDir stringByAppendingPathComponent:@"savedVenue.plist"];
    
    self.favVenues = [NSKeyedUnarchiver unarchiveObjectWithFile:fullPath];
    
    if (!self.favVenues) {
        self.favVenues = [[NSMutableArray alloc]init];
    }
    
    // Dit zorgt ervoor dat het sterretje in het begin wordt toegevoegd

    for (VenueItem *venue in self.favVenues) {
        if ([venue.title isEqualToString:self.myVenue.title])
            self.hideFavoButton.selected = YES;
    }
    
    NSDictionary *originalPlist = [NSDictionary dictionaryWithContentsOfFile:fullPath];
    [originalPlist writeToFile:fullPath atomically:YES];
    
    
}



- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    self.hideFavoButton.selected = selected;
}


- (IBAction)addToFavoInfoCard:(id)sender

{
//    NSMutableArray *favorieten = [[VenueStore sharedStore] favorieten];
//    //
//    //    [favorieten addObject:self.myVenue];
//    
//    NSLog(@"toegevoegde venue:: %@", self.myVenue);
    
    // NSLog(@"favorieten:: %@", favorieten);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDir stringByAppendingPathComponent:@"savedVenue.plist"];
    
    
    
    if (self.hideFavoButton.selected) {
        //Verwijder uit favorieten
        
       NSMutableArray *toDelete = [NSMutableArray array];
        
        for (VenueItem *item in self.favVenues) {
            if ([item.title isEqualToString:self.myVenue.title]) {
                [toDelete  addObject:item];
                
            }
            
        }
        [self.favVenues removeObjectsInArray:toDelete];
        
        
        NSLog(@"dit is favvenuess: %@", self.favVenues);
        
        NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:self.favVenues];
        [archiveData writeToFile:fullPath options:NSDataWritingAtomic error:nil];
        self.hideFavoButton.selected = NO;


    } else {
                [self.favVenues addObject:self.myVenue];
                NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:self.favVenues];
         [archiveData writeToFile:fullPath options:NSDataWritingAtomic error:nil];
        self.hideFavoButton.selected = YES;
        
        NSLog(@"dit iss favvenues na toevoegen: %@", self.favVenues);
        
    
    
//   self.hideFavoButton.selected = !self.hideFavoButton.selected;
//
//    if (self.hideFavoButton.selected) {
//          [self.hideFavoButton setBackgroundImage:[UIImage imageNamed:@"favorite_on_106x43_.png"] forState:UIControlStateNormal];
//        
//        [favorieten addObject:self.myVenue];    }
//    else {
//        [favorieten removeObject:self.myVenue];
//        
// [self.hideFavoButton setBackgroundImage:[UIImage imageNamed:@"favorite_off_106x43_.png"] forState:UIControlStateNormal];    }
//
    }

}




- (IBAction)callInfoCard:(id)sender {
    
    NSString* infoName = self.myVenue.title;


    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Do you really want to call" ]
                                                     message:[NSString stringWithFormat:@" %@ ?", infoName ]
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Yes", nil] self];
    [alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) { // Set buttonIndex == 0 to handel "Ok"/"Yes" button response
        
        NSString* phone = self.myVenue.telefoon;
        NSString* urlString = [NSString stringWithFormat: @"tel://%@", phone];
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: urlString]];
        
        
        // Cancel button response
    }
}




- (IBAction)websiteButton:(id)sender {
    NSLog(@"Button Clicked");
    NSString* website = self.myVenue.website;
    NSString* webString = [NSString stringWithFormat: @"%@", website];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:webString]];
}


@end
