//
//  ImageStore.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageStore : NSObject

+ (instancetype)sharedStore;

+ (NSArray *)categoriesIcons;
+ (UIImage *) categoryImageAtIndex:(NSInteger) index;


@end
