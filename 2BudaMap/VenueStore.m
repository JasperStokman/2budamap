//
//  VenueStore.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "VenueStore.h"
#import "VenueItem.h"

@interface VenueStore()
@property  (nonatomic) NSMutableArray* allVenues;
@property (nonatomic)NSMutableArray* fVenues;

@end
@implementation VenueStore

//toegevoegd
-(NSArray *)allVenuesList
{
    return self.allVenues;
}

- (NSArray *)allItems
{
    return self.favorieten;
}


- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.favorieten];
   
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _favorieten = [aDecoder decodeObjectForKey:@"title"];
      
    }
    return self;
}


//- (NSString *)itemArchivePath
//{
//    // Make sure that the first argument is NSDocumentDirectory
//    // and not NSDocumentationDirectory
//    NSArray *documentDirectories =
//    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
//                                        NSUserDomainMask, YES);
//    
//    // Get the one document directory from that list
//    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
//    NSString *fullPath = [documentDirectory stringByAppendingPathComponent:@"savedVenues.plist"];
//    
//    
//    return fullPath;
//}
//
//
//- (BOOL)saveChanges
//{
//    NSString *path = [self itemArchivePath];
//    
//
//    // Returns YES on success
//    return [NSKeyedArchiver archiveRootObject:self.fVenues
//                                       toFile:path];
//}



+ (instancetype)sharedStore
{
    static VenueStore *sharedStore;
    
    
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    
    return sharedStore;
}



- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[VenueStore sharedStore]"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        _favorieten = [[NSMutableArray alloc] init];
        //toegevoegd
        _allVenues = [[NSMutableArray alloc] init];
        [self retrievieVenues];
    }
    return self;
}

// Dit leest de JSON
- (NSArray *) loadJSON: (NSString *)fileName
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    //NSLog(@"filepath: %@", filePath);
   
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //NSLog(@"data: %@", data);

    NSError *error;
    NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    NSLog(@"error: %@", error);

    
    return jsonObject;
}


- (void) retrievieVenues
{
    //onderstaand uit gemonteraieerd
    //NSMutableArray *venues = [[NSMutableArray alloc] init];
    //NSArray *jsonObject = [self loadJSON:@"test"];
    self.jsonObject = [self loadJSON:@"venue"];
    
    for (NSDictionary *venueJSON in _jsonObject) {
        VenueItem *venue = [[VenueItem alloc]init];
        
        venue.venueType = venueJSON[@"Category"];
        venue.SubType = venueJSON[@"SubType"];
        venue.website = venueJSON[@"website"];
        venue.adres = venueJSON[@"adres"];
        venue.telefoon = venueJSON[@"telefoonnummer"];
        venue.title = venueJSON[@"Title"];
        venue.omschrijving = venueJSON[@"info"];
        venue.email = venueJSON[@"email"];
        double lat = [venueJSON[@"latitude"] doubleValue];
        double lon = [venueJSON[@"longitude"] doubleValue];
        venue.coordinate = CLLocationCoordinate2DMake(lat, lon);
        
        [_allVenues addObject:venue];
    }

    _venueList = [NSArray arrayWithArray:self.allVenues];
   // NSLog(@"%@", self.allVenuesList);
    //_allVenues was: venues
}


- (NSArray *)categories
{
    NSMutableSet *venueTypes = [[NSMutableSet alloc] init];
    
    for (VenueItem *item in self.venueList) {
        
        [venueTypes addObject:item.venueType];
        
    }
    
    

    NSMutableArray *venueTypeList = [[venueTypes allObjects] mutableCopy];
    
    return venueTypeList;
    
    

}


//- (NSArray *)allCategories
//{
//    
//    NSMutableArray *venueTypes = [[NSMutableArray alloc]init];
//    
//    for (VenueItem *item in self.venueList) {
//        [venueTypes addObject:item.venueType];
//        
//    }
//    
//    return venueTypes;
//}




- (NSArray *)giveSubtypesForCategory: (NSString *)category
{
    NSMutableSet *subTypes = [[NSMutableSet alloc]init];

    for (VenueItem *myItem in self.venueList) {
        if ([myItem.venueType isEqualToString:category]) {
           
            
            [subTypes addObject:myItem.subType];
        }
    }
    
    return [subTypes allObjects];
}


- (NSArray *)giveTopVenuesForSubtype: (NSString *)subType
{
    NSMutableSet *subTypes = [[NSMutableSet alloc]init];
    
    for (VenueItem *myItem in self.venueList) {
        if ([myItem.subType isEqualToString:subType]) {
            //NSLog(@"venueType: %@",myItem.venueType);
            //NSLog(@"subType: %@",myItem.subType);
            
            [subTypes addObject:myItem];
        }
    }
    
    return [subTypes allObjects];
}






// telefoon lijstje **************

// Algemene Telefoon nummers in lijstje belangrijke telefoon nummers..
- (NSArray *)telNummersAlgemeen {
    return [NSArray arrayWithObjects:@"112", @"+3614388080", @"+3612111111", @"+3612222222", @"+3614579777", @"+3614797010", @"+3614579960", @"+3614879000", @"+3613741100", @"+3614883500", @"+3613014960", @"+3614606200", @"+3613366300", @"+3612017617", @"+3613429992", @"+3614607040", @" +3612662888", @"+3614754400",nil];

}

// Namen van de algemene telefoon nummers...
- (NSArray *)telNamenAlgemeen {
    
    return [NSArray arrayWithObjects:@"Emergency", @"Crime Hotline", @"City Taxi", @"Fo Taxi", @"Australian Embassy", @"Austrian Embassy", @"Belgian Embassy", @"Danish Embassy"  ,@"French Embassy",@"German Embassy",@"Irish Embassy",@"Italian Embassy",@"Dutch Embassy",@"Portuguese Embassy",@"Spanish Embassy",@"Swiss Embassy",@"British Embassy",@"American Embassy",nil];
    
}

//// andere sectie..
//- (NSArray *)telNummersAnders {
//    return [NSArray arrayWithObjects:@"107", @"105", @"104", @"112",nil];
//    
//}
//
//- (NSArray *)telNamenAnders {
//    
//    return [NSArray arrayWithObjects:@"Police", @"Fire department", @"Ambulance Service", @"Emergency number",nil];
//
//}



// Einde telefoon lijstje **************


- (void) vulMetNepData
{
    NSMutableArray *nepgegevens = [NSMutableArray array];
    VenueItem *nep = [VenueItem createNepVenue];
    [nepgegevens addObject:nep];
    self.venueList = [NSArray arrayWithArray:nepgegevens];
}

- (void)moveItemAtIndex:(NSInteger)fromIndex
                toIndex:(NSInteger)toIndex
{
    if (fromIndex == toIndex) {
        return;
    }
    // Get pointer to object being moved so you can re-insert it
    VenueItem *item = self.venueList[fromIndex];
    
    // Remove item from array
    [self.favorieten removeObjectAtIndex:fromIndex];
    
    // Insert item in array at new location
    [self.favorieten insertObject:item atIndex:toIndex];
    
    
}

- (NSMutableArray *)fVenues {
    if (!_fVenues) {
        _fVenues = [[NSMutableArray alloc]init];
    }
    return _fVenues;
    
    
}




@end
