//
//  VenueStore.h
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueStore : NSObject <NSCoding>
+ (instancetype)sharedStore;

@property (nonatomic) NSMutableArray *favorieten;
@property (nonatomic) NSArray *telLijst;
@property (nonatomic) NSArray *venueList;
@property (nonatomic) NSArray *jsonObject;
//@property (nonatomic) NSMutableArray* venues;



// telnummers & namen //
@property (nonatomic) NSArray *telNummersAlgemeen;
@property (nonatomic) NSArray *telNamenAlgemeen;


//@property (nonatomic) NSArray *telNummersAnders;
//@property (nonatomic) NSArray *telNamenAnders;
// **** einde tele nummers **** //


- (NSArray *) loadJSON: (NSString *)fileName;

- (NSArray *)allVenuesList;
- (NSArray *)categories;
- (NSArray *)allCategories;

// test
//- (NSArray *)nepDataDave;


// lijst met subcategorieen..
- (NSArray *)giveSubtypesForCategory: (NSString *)category;

- (NSArray *)giveTopVenuesForSubtype: (NSString *)subType;



//- (BOOL)saveChanges;


@end
