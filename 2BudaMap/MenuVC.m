//
//  MenuVC.m
//  2BudaMap
//
//  Created by Camille Bruma on 22/05/14.
//  Copyright (c) 2014 TheAppAcademy. All rights reserved.
//

#import "MenuVC.h"
#import "MenuTableViewCell.h"
#import "SidePanelController.h"
#import "JASidePanelController.h"
#import "MenuDetailVC.h"
#import "UIViewController+JASidePanel.h"


@interface MenuVC ()

@end

@implementation MenuVC

{
    NSArray *menuData;
    NSArray *menuIcons;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Initialize table data
    menuData = [NSArray arrayWithObjects:@"EXPLORE BUDAPEST", @"FAVORITES", @"TELEPHONE NUMBERS", @"2budapest.com", nil];
    
    // Initialize thumbnails
    menuIcons = [NSArray arrayWithObjects:@"map.png", @"favorite.png", @"telephone.png", @"web.png",  nil];
   
    
    
}



//drag from the viewController (Small orange icon underneath the View) to the next viewController. Then make sure you give the segue an identifier then use:
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"infoKaartBalk-1_C3000B.png"]];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor redColor];
    [cell setSelectedBackgroundView:bgColorView];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSString *page;
    switch (indexPath.row) {
        case 0:
            page = @"MapsPanel";
            break;
        case 1:
            page = @"FavorietPanel";
            break;
        case 2:
            page = @"TelefoonNRPanel";
            break;
        case 3:
            page = @"InstellingenPanel";
            break;
     
            
    }
   self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:page];

}



//(Copy and paste your segue identifier to alleviate any spelling mistakes).



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MenuTableViewCell";
    
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.menuTitle.text = [menuData objectAtIndex:indexPath.row];
    cell.menuTitle.textColor = [UIColor whiteColor];
    cell.menuTitle.font = [UIFont fontWithName:@"Novecentowide-Medium" size:15];
    cell.menuIcon.image = [UIImage imageNamed:[menuIcons objectAtIndex:indexPath.row]];
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuData count];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectie = [self.menuTableView indexPathForSelectedRow];
    NSString *title = [menuData objectAtIndex:selectie.row];
//    NSLog(@"%@", title);
    MenuDetailVC *segueDestination = segue.destinationViewController;
    segueDestination.selectedTitle = title;
    
}

@end
